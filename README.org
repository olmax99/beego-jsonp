#+TITLE: beego-jsonp
#+SETUPFILE: ~/s3sync/org/conf/setup.config
#+FILETAGS: :golang:beego:web:jsonp:html

Follow the [[https://astaxie.gitbooks.io/build-web-application-with-golang/content/en/14.1.html][Beego JSONP Guide]].

* Prerequisites
- Beego v2.0.2
- Go 1.16.x

* Commands
#+begin_src bash
# RUN
$ go build
$ ./beego-jsonp 
#+end_src

* Testing
..

* General Instructions
..

* Where to go from here?
..
