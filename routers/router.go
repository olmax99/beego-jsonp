package routers

import (
	"beego-jsonp/controllers"

	beego "github.com/beego/beego/v2/server/web"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	//Api interface section
	beego.Router("/api/Html", &controllers.ApiController{})
	beego.Router("/api/GetJsonp", &controllers.ApiJsonpController{})
}
