package controllers

import (
	beego "github.com/beego/beego/v2/server/web"
)

type ApiController struct {
	beego.Controller
}

func (c *ApiController) Get() {
	c.TplName = "oneApi.html"
}

//Data in Jsonp format
type ApiJsonpController struct {
	beego.Controller
}

func (c *ApiJsonpController) Get() {
	//Note that the jsonp here must be jsonp
	c.Data["jsonp"] = "CDEFGHI"
	c.ServeJSONP()
}
